package ru.t1.aksenova.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.aksenova.tm.entity.dto.ProjectDTO;

import java.util.List;

public interface ProjectRestEndpointClient {

    @NotNull
    String BASE_URL = "http://localhost:8080/api/projects";

    static ProjectRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectRestEndpointClient.class, BASE_URL);
    }

    @GetMapping("/findAll")
    List<ProjectDTO> findAll();

    @PostMapping("/add")
    ProjectDTO add(@RequestBody ProjectDTO task);

    @PostMapping("/save")
    ProjectDTO save(@RequestBody ProjectDTO project);

    @GetMapping("/findById/{id}")
    ProjectDTO findById(@PathVariable("id") String id);

    @GetMapping("/existsById/{id}")
    boolean existsById(@PathVariable("id") String id);

    @GetMapping("/count")
    long count();

    @PostMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") String id);

    @PostMapping("/delete")
    void delete(@RequestBody ProjectDTO project);

    @PostMapping("/deleteAll")
    void clear();

}
