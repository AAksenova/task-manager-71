package ru.t1.aksenova.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.entity.model.User;
import ru.t1.aksenova.tm.enumerated.RoleType;

public interface IUserService {
    void create(
            @Nullable String login,
            @Nullable String password,
            @Nullable RoleType roleType);

    void save(@Nullable User user);

    User findByLogin(@Nullable String login);

    boolean existsByLogin(@Nullable String login);
}
