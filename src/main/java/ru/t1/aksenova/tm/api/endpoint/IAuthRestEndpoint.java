package ru.t1.aksenova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.t1.aksenova.tm.entity.dto.Result;
import ru.t1.aksenova.tm.entity.model.User;

@RequestMapping("/api/auth")
public interface IAuthRestEndpoint {

    @NotNull
    @PostMapping("/login")
    Result login(@NotNull @RequestParam("login") String username, @NotNull @RequestParam("password") String password);

    @Nullable
    @GetMapping("/profile")
    User profile();

    @NotNull
    @PostMapping("/logout")
    Result logout();

}
