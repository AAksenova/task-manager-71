package ru.t1.aksenova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.t1.aksenova.tm.api.service.dto.IUserService;
import ru.t1.aksenova.tm.entity.model.Role;
import ru.t1.aksenova.tm.entity.model.User;
import ru.t1.aksenova.tm.enumerated.RoleType;
import ru.t1.aksenova.tm.exception.entity.UserNotFoundException;
import ru.t1.aksenova.tm.exception.field.LoginEmptyException;
import ru.t1.aksenova.tm.exception.field.PasswordEmptyException;
import ru.t1.aksenova.tm.exception.user.ExistLoginException;
import ru.t1.aksenova.tm.exception.user.RoleEmptyException;
import ru.t1.aksenova.tm.repository.model.UserRepository;

import java.util.Collections;

@Service
public class UserService implements IUserService {

    @NotNull
    @Autowired
    private UserRepository userRepository;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable RoleType roleType) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (userRepository.existsByLogin(login)) throw new ExistLoginException();
        if (roleType == null) throw new RoleEmptyException();
        @NotNull final String passwordHash = passwordEncoder.encode(password);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        @NotNull final Role role = new Role(user, roleType);
        user.setRoles(Collections.singletonList(role));
        userRepository.save(user);
    }

    @Override
    public void save(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        userRepository.saveAndFlush(user);
    }

    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean existsByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return userRepository.existsByLogin(login);
    }

}
