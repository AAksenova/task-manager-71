package ru.t1.aksenova.tm.enumerated;

public enum RoleType {

    ADMINISTRATOR,
    USER;

}