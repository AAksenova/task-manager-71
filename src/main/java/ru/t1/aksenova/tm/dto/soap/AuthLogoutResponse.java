package ru.t1.aksenova.tm.dto.soap;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.entity.dto.Result;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlType(name = "", propOrder = {
        "result"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "authLogoutResponse")
public class AuthLogoutResponse {

    @NotNull
    @XmlElement(name = "result")
    protected Result result;

}
