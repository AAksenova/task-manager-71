package ru.t1.aksenova.tm.dto.soap;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlType(name = "", propOrder = {
        "login",
        "password"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "authLoginRequest")
public class AuthLoginRequest {

    @NotNull
    @XmlElement(required = true)
    protected String login;

    @NotNull
    @XmlElement(required = true)
    protected String password;

}
