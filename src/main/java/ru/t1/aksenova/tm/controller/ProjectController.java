package ru.t1.aksenova.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.aksenova.tm.api.service.dto.IProjectDTOService;
import ru.t1.aksenova.tm.api.service.dto.ITaskDTOService;
import ru.t1.aksenova.tm.entity.dto.CustomUser;
import ru.t1.aksenova.tm.entity.dto.ProjectDTO;
import ru.t1.aksenova.tm.enumerated.Status;

@Controller
public class ProjectController {

    @Autowired
    private IProjectDTOService projectService;

    @Autowired
    private ITaskDTOService taskService;

    @GetMapping("project/create")
    @PreAuthorize("isAuthenticated()")
    public String create(@AuthenticationPrincipal @NotNull final CustomUser user) {
        projectService.add(new ProjectDTO(user.getUserId(), "New Project " + System.currentTimeMillis(), Status.NOT_STARTED));
        return "redirect:/projects";
    }

    @GetMapping("project/delete/{id}")
    @PreAuthorize("isAuthenticated()")
    public String delete(@AuthenticationPrincipal @NotNull final CustomUser user, @PathVariable("id") String id) {
        taskService.removeByProjectId(user.getUserId(), id);
        projectService.removeOneById(user.getUserId(), id);
        return "redirect:/projects";
    }

    @PostMapping("project/edit/{id}")
    @PreAuthorize("isAuthenticated()")
    public String edit(@AuthenticationPrincipal @NotNull final CustomUser user,
                       @ModelAttribute("project") ProjectDTO project,
                       BindingResult result
    ) {
        project.setUserId(user.getUserId());
        projectService.update(project);
        return "redirect:/projects";
    }

    @GetMapping("project/edit/{id}")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView edit(@AuthenticationPrincipal @NotNull final CustomUser user,
                             @PathVariable("id") String id
    ) {
        final ProjectDTO project = projectService.findOneById(user.getUserId(), id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", getStatuses());
        return modelAndView;
    }

    public Status[] getStatuses() {
        return Status.values();
    }

}
