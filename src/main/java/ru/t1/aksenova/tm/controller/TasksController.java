package ru.t1.aksenova.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.aksenova.tm.api.service.dto.IProjectDTOService;
import ru.t1.aksenova.tm.api.service.dto.ITaskDTOService;
import ru.t1.aksenova.tm.entity.dto.CustomUser;

@Controller
public class TasksController {

    @Autowired
    private ITaskDTOService taskService;

    @Autowired
    private IProjectDTOService projectService;

    @GetMapping("/tasks")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView index(@AuthenticationPrincipal @NotNull final CustomUser user) {
        ModelAndView modelAndView = new ModelAndView("task-list", "tasks", taskService.findAll(user.getUserId()));
        modelAndView.addObject("projectService", projectService);
        return modelAndView;
    }

}
