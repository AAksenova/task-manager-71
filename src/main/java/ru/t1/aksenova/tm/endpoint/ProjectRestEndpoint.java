package ru.t1.aksenova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.bind.annotation.*;
import ru.t1.aksenova.tm.api.endpoint.IProjectRestEndpoint;
import ru.t1.aksenova.tm.api.service.dto.IProjectDTOService;
import ru.t1.aksenova.tm.entity.dto.ProjectDTO;
import ru.t1.aksenova.tm.util.UserUtil;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Override
    @PostMapping("/add")
    @PreAuthorize("isAuthenticated()")
    public ProjectDTO add(@RequestBody final @NotNull ProjectDTO project) {
        project.setUserId(UserUtil.getUserId());
        return projectService.add(project);
    }

    @Nullable
    @Override
    @GetMapping("/findAll")
    @PreAuthorize("isAuthenticated()")
    public List<ProjectDTO> findAll() {
        return projectService.findAll(UserUtil.getUserId());
    }

    @Override
    @PostMapping("/save")
    @PreAuthorize("isAuthenticated()")
    public void save(@RequestBody final @NotNull ProjectDTO project) {
        project.setUserId(UserUtil.getUserId());
        projectService.update(project);
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    @PreAuthorize("isAuthenticated()")
    public ProjectDTO findById(@PathVariable("id") final @NotNull String id) {
        ProjectDTO projectDTO = projectService.findOneById(UserUtil.getUserId(), id);
        return projectDTO;
    }

    @Override
    @GetMapping("/existsById/{id}")
    @PreAuthorize("isAuthenticated()")
    public boolean existsById(@PathVariable("id") final @NotNull String id) {
        ProjectDTO p = projectService.findOneById(UserUtil.getUserId(), id);
        return (projectService.findOneById(UserUtil.getUserId(), id) != null);
    }

    @Override
    @GetMapping("/count")
    @PreAuthorize("isAuthenticated()")
    public long count() {
        return projectService.getSize(UserUtil.getUserId());
    }

    @Override
    @PostMapping("/deleteById/{id}")
    @PreAuthorize("isAuthenticated()")
    public void deleteById(@PathVariable("id") final @NotNull String id) {
        projectService.removeOneById(UserUtil.getUserId(), id);
    }

    @Override
    @PostMapping("/delete")
    @PreAuthorize("isAuthenticated()")
    public void delete(@RequestBody final @NotNull ProjectDTO project) {
        project.setUserId(UserUtil.getUserId());
        projectService.remove(project);
    }

    @Override
    @PostMapping("/deleteAll")
    @PreAuthorize("isAuthenticated()")
    public void clear() {
        projectService.removeAll(UserUtil.getUserId());
    }

}

