package ru.t1.aksenova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.t1.aksenova.tm.api.endpoint.IAuthRestEndpoint;
import ru.t1.aksenova.tm.api.service.dto.IUserService;
import ru.t1.aksenova.tm.entity.dto.Result;
import ru.t1.aksenova.tm.entity.model.User;

@RestController
@RequestMapping("/api/auth")
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class AuthRestEndpoint implements IAuthRestEndpoint {

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Override
    @PostMapping("/login")
    @PreAuthorize("!isAuthenticated()")
    public Result login(@NotNull @RequestParam("login") final String username,
                        @NotNull @RequestParam("password") final String password
    ) {
        try {
            @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            @NotNull final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (@NotNull final Exception e) {
            return new Result(e);
        }
    }

    @Nullable
    @Override
    @GetMapping("/profile")
    @PreAuthorize("isAuthenticated()")
    public User profile() {
        @NotNull final SecurityContext securityContext = SecurityContextHolder.getContext();
        @Nullable final Authentication authentication = securityContext.getAuthentication();
        if (authentication == null) return null;
        @NotNull final String username = authentication.getName();
        return userService.findByLogin(username);
    }

    @NotNull
    @Override
    @PostMapping("/logout")
    @PreAuthorize("isAuthenticated()")
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

}

