package ru.t1.aksenova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.bind.annotation.*;
import ru.t1.aksenova.tm.api.endpoint.ITaskRestEndpoint;
import ru.t1.aksenova.tm.api.service.dto.ITaskDTOService;
import ru.t1.aksenova.tm.entity.dto.TaskDTO;
import ru.t1.aksenova.tm.util.UserUtil;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private ITaskDTOService taskService;

    @NotNull
    @Override
    @PostMapping("/add")
    @PreAuthorize("isAuthenticated()")
    public TaskDTO add(@RequestBody final @NotNull TaskDTO task) {
        task.setUserId(UserUtil.getUserId());
        return taskService.add(task);
    }

    @Nullable
    @Override
    @GetMapping("/findAll")
    @PreAuthorize("isAuthenticated()")
    public List<TaskDTO> findAll() {
        return taskService.findAll(UserUtil.getUserId());
    }

    @Override
    @PostMapping("/save")
    @PreAuthorize("isAuthenticated()")
    public void save(@RequestBody final @NotNull TaskDTO task) {
        task.setUserId(UserUtil.getUserId());
        taskService.update(task);
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    @PreAuthorize("isAuthenticated()")
    public TaskDTO findById(@PathVariable("id") final @NotNull String id) {
        return taskService.findOneById(UserUtil.getUserId(), id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    @PreAuthorize("isAuthenticated()")
    public boolean existsById(@PathVariable("id") final @NotNull String id) {
        return (taskService.findOneById(UserUtil.getUserId(), id) != null);
    }

    @Override
    @GetMapping("/count")
    @PreAuthorize("isAuthenticated()")
    public long count() {
        return taskService.getSize(UserUtil.getUserId());
    }

    @Override
    @PostMapping("/deleteById/{id}")
    @PreAuthorize("isAuthenticated()")
    public void deleteById(@PathVariable("id") final @NotNull String id) {
        taskService.removeOneById(UserUtil.getUserId(), id);
    }

    @Override
    @PostMapping("/delete")
    @PreAuthorize("isAuthenticated()")
    public void delete(@RequestBody final @NotNull TaskDTO task) {
        task.setUserId(UserUtil.getUserId());
        taskService.remove(task);
    }

    @Override
    @PostMapping("/deleteAll")
    @PreAuthorize("isAuthenticated()")
    public void clear() {
        taskService.removeAll(UserUtil.getUserId());
    }

}
