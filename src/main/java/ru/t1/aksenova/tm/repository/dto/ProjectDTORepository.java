package ru.t1.aksenova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aksenova.tm.entity.dto.ProjectDTO;

import java.util.List;
import java.util.Optional;

@Repository
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public interface ProjectDTORepository extends JpaRepository<ProjectDTO, String> {

    @NotNull
    List<ProjectDTO> findAllByUserId(@NotNull String userId);

    @NotNull
    Optional<ProjectDTO> findByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

    @Transactional
    void deleteAllByUserId(@NotNull String userId);

}
