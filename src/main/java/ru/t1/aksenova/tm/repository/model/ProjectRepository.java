package ru.t1.aksenova.tm.repository.model;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.aksenova.tm.entity.model.Project;

@Repository
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public interface ProjectRepository extends JpaRepository<Project, String> {

}
