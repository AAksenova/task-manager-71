package ru.t1.aksenova.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.aksenova.tm.config.*;
import ru.t1.aksenova.tm.entity.dto.ProjectDTO;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.util.UserUtil;

import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.t1.aksenova.tm.constant.ProjectTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.TEST_LOGIN;
import static ru.t1.aksenova.tm.constant.UserTestData.TEST_PASSWORD;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        ApplicationConfiguration.class,
        ServerConfiguration.class,
        SecurityWebApplicationInitializer.class,
        ServiceAuthenticationEntryPoint.class,
        WebApplicationConfiguration.class
})
public class ProjectRestEndpointTest {

    @NotNull
    private static final ProjectDTO projectOne = new ProjectDTO(ONE_PROJECT_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @NotNull
    private static final ProjectDTO projectTwo = new ProjectDTO(TWO_PROJECT_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @NotNull
    private static final ProjectDTO projectThree = new ProjectDTO(THREE_PROJECT_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @Nullable
    private String userId;

    private boolean reqAuth = false;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        authenticate();
        userId = UserUtil.getUserId();
        projectOne.setUserId(userId);
        projectTwo.setUserId(userId);
        projectThree.setUserId(userId);
        save(projectOne);
        save(projectTwo);
    }

    @After
    public void tearDown() throws Exception {
        if (reqAuth) {
            authenticate();
        }
        reqAuth = false;
        delete(projectOne);
        delete(projectTwo);
        delete(projectThree);
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    private void authenticate() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(TEST_LOGIN, TEST_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @SneakyThrows
    private void save(@NotNull final ProjectDTO project) {
        @NotNull final String url = PROJECT_URL + "save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void saveTest() {
        @Nullable ProjectDTO project = findById(projectTwo.getId());
        Assert.assertNotNull(project);
        project.setDescription(TWO_PROJECT_DESCRIPTION);
        project.setStatus(Status.valueOf(STATUS_IN_PROGRESS));
        save(project);
        @Nullable ProjectDTO project2 = findById(projectTwo.getId());
        Assert.assertNotNull(project2);
        Assert.assertEquals(project.getDescription(), project2.getDescription());
        Assert.assertEquals(project.getStatus(), project2.getStatus());
    }

    @SneakyThrows
    public void add(@NotNull final ProjectDTO project) {
        @NotNull final String url = PROJECT_URL + "add";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project);
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void addTest() {
        add(projectThree);
        @NotNull final String projectName = projectThree.getName();
        @Nullable final ProjectDTO project = findById(projectThree.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(projectName, project.getName());
    }

    @SneakyThrows
    public List<ProjectDTO> findAll() {
        @NotNull final String url = PROJECT_URL + "findAll";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, ProjectDTO[].class));
    }

    @Test
    public void findAllTest() {
        final long projectSize = count();
        @Nullable final List<ProjectDTO> projects = findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectSize, projects.size());
        projects.forEach(project -> Assert.assertNotNull(project.getId()));
    }

    @SneakyThrows
    public ProjectDTO findById(@NotNull final String id) {
        @NotNull final String url = PROJECT_URL + "findById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if ("".equals(json)) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, ProjectDTO.class);
    }

    @Test
    public void findByIdTest() {
        @NotNull final String projectId = projectOne.getId();
        @Nullable final ProjectDTO project = findById(projectId);
        Assert.assertNotNull(project);
        Assert.assertEquals(projectId, project.getId());
    }

    @SneakyThrows
    private void delete(@NotNull final ProjectDTO project) {
        @NotNull final String url = PROJECT_URL + "delete";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project);
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void deleteTest() {
        Assert.assertNotNull(findById(projectOne.getId()));
        delete(projectOne);
        Assert.assertThrows(Exception.class, () -> findById(projectOne.getId()));
        add(projectOne);
    }

    @SneakyThrows
    private void deleteById(@NotNull final String id) {
        @NotNull final String url = PROJECT_URL + "deleteById/" + id;
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void deleteByIdTest() {
        @NotNull final String projectId = projectOne.getId();
        Assert.assertNotNull(findById(projectId));
        deleteById(projectId);
        Assert.assertThrows(Exception.class, () -> findById(projectId));
        add(projectOne);
    }

    @SneakyThrows
    private long count() {
        @NotNull final String url = PROJECT_URL + "count";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Long.class);
    }

    @Test
    public void countTest() {
        final long projectSize = count();
        @Nullable final List<ProjectDTO> projects = findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectSize, projects.size());
    }

    @SneakyThrows
    private boolean existsById(@NotNull final String id) {
        @NotNull final String url = PROJECT_URL + "existsById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Boolean.class);
    }

    @Test
    public void existsByIdTest() {
        Assert.assertTrue(existsById(projectTwo.getId()));
        @NotNull final ProjectDTO projectFour = new ProjectDTO(FOUR_PROJECT_NAME, Status.valueOf(STATUS_COMPLETE));
        Assert.assertThrows(Exception.class, () -> existsById(projectFour.getId()));
    }

    @SneakyThrows
    private void deleteAll() {
        @NotNull final String url = PROJECT_URL + "deleteAll";
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void deleteAllTest() {
        @Nullable final List<ProjectDTO> projects = findAll();
        Assert.assertNotNull(projects);
        deleteAll();
        @Nullable final List<ProjectDTO> projects2 = findAll();
        Assert.assertNotNull(projects2);
        Assert.assertEquals(Collections.emptyList(), projects2);
        Assert.assertEquals(0, projects2.size());
        for (@NotNull ProjectDTO project : projects) {
            add(project);
        }
    }

}
