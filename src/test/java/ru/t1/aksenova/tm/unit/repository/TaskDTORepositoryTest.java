package ru.t1.aksenova.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.aksenova.tm.config.ServerConfiguration;
import ru.t1.aksenova.tm.entity.dto.ProjectDTO;
import ru.t1.aksenova.tm.entity.dto.TaskDTO;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.repository.dto.ProjectDTORepository;
import ru.t1.aksenova.tm.repository.dto.TaskDTORepository;
import ru.t1.aksenova.tm.util.UserUtil;

import java.util.List;

import static ru.t1.aksenova.tm.constant.ProjectTestData.ONE_PROJECT_NAME;
import static ru.t1.aksenova.tm.constant.ProjectTestData.STATUS_NOT_STARTED;
import static ru.t1.aksenova.tm.constant.TaskTestData.ALFA_TASK_NAME;
import static ru.t1.aksenova.tm.constant.TaskTestData.BETTA_TASK_NAME;
import static ru.t1.aksenova.tm.constant.UserTestData.TEST_LOGIN;
import static ru.t1.aksenova.tm.constant.UserTestData.TEST_PASSWORD;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ServerConfiguration.class})
public class TaskDTORepositoryTest {

    @NotNull
    private static final ProjectDTO projectOne = new ProjectDTO(ONE_PROJECT_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @NotNull
    private static final TaskDTO taskOne = new TaskDTO(ALFA_TASK_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @NotNull
    private static final TaskDTO taskTwo = new TaskDTO(BETTA_TASK_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @Nullable
    private String userId;

    @NotNull
    @Autowired
    private ProjectDTORepository projectRepository;

    @NotNull
    @Autowired
    private TaskDTORepository taskRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void setUp() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(TEST_LOGIN, TEST_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        projectOne.setUserId(userId);
        projectRepository.save(projectOne);
        taskOne.setProjectId(projectOne.getId());
        taskOne.setUserId(userId);
        taskTwo.setUserId(userId);
        taskRepository.save(taskOne);
        taskRepository.save(taskTwo);
    }

    @After
    public void tearDown() {
        taskRepository.delete(taskOne);
        taskRepository.delete(taskTwo);
        projectRepository.delete(projectOne);
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    @Test
    public void findAllByUserIdTest() {
        @Nullable final List<TaskDTO> tasks = taskRepository.findAllByUserId(userId);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    public void findByUserIdAndIdTest() {
        Assert.assertNotNull(taskRepository.findByUserIdAndId(userId, taskOne.getId()));
    }

    @Test
    public void countByUserIdTest() {
        Assert.assertEquals(2, taskRepository.countByUserId(userId));
    }

    @Test
    public void deleteByUserIdAndProjectIdTest() {
        @Nullable final TaskDTO task = taskRepository.findByUserIdAndId(userId, taskOne.getId()).get();
        Assert.assertNotNull(task);
        Assert.assertEquals(projectOne.getId(), task.getProjectId());
        taskRepository.deleteByUserIdAndProjectId(userId, projectOne.getId());
        Assert.assertFalse(taskRepository.findByUserIdAndId(userId, taskOne.getId()).isPresent());
    }

    @Test
    public void deleteAllByUserIdTest() {
        taskRepository.deleteAllByUserId(userId);
        @Nullable final List<TaskDTO> tasks = taskRepository.findAllByUserId(userId);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(0, tasks.size());
        taskRepository.save(taskOne);
        taskRepository.save(taskTwo);
    }

}