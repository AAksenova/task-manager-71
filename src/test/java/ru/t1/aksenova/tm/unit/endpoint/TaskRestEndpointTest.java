package ru.t1.aksenova.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.aksenova.tm.config.ServerConfiguration;
import ru.t1.aksenova.tm.config.WebApplicationConfiguration;
import ru.t1.aksenova.tm.entity.dto.TaskDTO;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.util.UserUtil;

import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.t1.aksenova.tm.constant.ProjectTestData.*;
import static ru.t1.aksenova.tm.constant.TaskTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.TEST_LOGIN;
import static ru.t1.aksenova.tm.constant.UserTestData.TEST_PASSWORD;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, ServerConfiguration.class})
public class TaskRestEndpointTest {

    @NotNull
    private static final TaskDTO taskOne = new TaskDTO(ALFA_TASK_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @NotNull
    private static final TaskDTO taskTwo = new TaskDTO(BETTA_TASK_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @NotNull
    private static final TaskDTO taskThree = new TaskDTO(GAMMA_TASK_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @Nullable
    private String userId;

    private boolean reqAuth = false;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        authenticate();
        userId = UserUtil.getUserId();
        taskOne.setUserId(userId);
        taskTwo.setUserId(userId);
        taskThree.setUserId(userId);
        save(taskOne);
        save(taskTwo);
    }

    @After
    public void tearDown() {
        if (reqAuth) {
            authenticate();
        }
        reqAuth = false;
        delete(taskOne);
        delete(taskTwo);
        delete(taskThree);
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    private void authenticate() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(TEST_LOGIN, TEST_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @SneakyThrows
    public void add(@NotNull final TaskDTO task) {
        @NotNull final String url = TASK_URL + "add";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task);
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void addTest() {
        add(taskThree);
        @NotNull final String taskName = taskThree.getName();
        @Nullable final TaskDTO task = findById(taskThree.getId());
        ;
        Assert.assertNotNull(task);
        Assert.assertEquals(taskName, task.getName());
    }

    @SneakyThrows
    private void save(@NotNull final TaskDTO task) {
        @NotNull final String url = TASK_URL + "save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void saveTest() {
        @Nullable TaskDTO task = findById(taskTwo.getId());
        Assert.assertNotNull(task);
        task.setDescription(BETTA_TASK_DESCRIPTION);
        task.setStatus(Status.valueOf(STATUS_IN_PROGRESS));
        save(task);
        @Nullable TaskDTO task2 = findById(taskTwo.getId());
        Assert.assertNotNull(task2);
        Assert.assertEquals(task.getDescription(), task2.getDescription());
        Assert.assertEquals(task.getStatus(), task2.getStatus());
    }

    @SneakyThrows
    public List<TaskDTO> findAll() {
        @NotNull final String url = TASK_URL + "findAll";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, TaskDTO[].class));
    }

    @Test
    public void findAllTest() {
        final long taskSize = count();
        @Nullable final List<TaskDTO> tasks = findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskSize, tasks.size());
        tasks.forEach(task -> Assert.assertNotNull(task.getId()));
    }

    @SneakyThrows
    public TaskDTO findById(@NotNull final String id) {
        @NotNull final String url = TASK_URL + "findById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, TaskDTO.class);
    }

    @Test
    public void findByIdTest() {
        @NotNull final String taskId = taskOne.getId();
        @Nullable final TaskDTO task = findById(taskId);
        Assert.assertNotNull(task);
        Assert.assertEquals(taskId, task.getId());
    }

    @SneakyThrows
    private void delete(@NotNull final TaskDTO project) {
        @NotNull final String url = TASK_URL + "delete";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project);
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void deleteTest() {
        Assert.assertNotNull(findById(taskOne.getId()));
        delete(taskOne);
        Assert.assertThrows(Exception.class, () -> findById(taskOne.getId()));
        add(taskOne);
    }

    @SneakyThrows
    private void deleteById(@NotNull final String id) {
        @NotNull final String url = TASK_URL + "deleteById/" + id;
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void deleteByIdTest() {
        @NotNull final String taskId = taskOne.getId();
        Assert.assertNotNull(findById(taskId));
        deleteById(taskId);
        Assert.assertThrows(Exception.class, () -> findById(taskId));
        add(taskOne);
    }

    @SneakyThrows
    private long count() {
        @NotNull final String url = TASK_URL + "count";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Long.class);
    }

    @Test
    public void countTest() {
        final long taskSize = count();
        @Nullable final List<TaskDTO> tasks = findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskSize, tasks.size());
    }

    @SneakyThrows
    private boolean existsById(@NotNull final String id) {
        @NotNull final String url = TASK_URL + "existsById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Boolean.class);
    }

    @Test
    public void existsByIdTest() {
        Assert.assertTrue(existsById(taskTwo.getId()));
        @NotNull final TaskDTO taskFour = new TaskDTO(DELTA_TASK_NAME, Status.valueOf(STATUS_COMPLETE));
        Assert.assertThrows(Exception.class, () -> existsById(taskFour.getId()));
    }

    @SneakyThrows
    private void deleteAll() {
        @NotNull final String url = TASK_URL + "deleteAll";
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void deleteAllTest() {
        @Nullable final List<TaskDTO> tasks = findAll();
        Assert.assertNotNull(tasks);
        deleteAll();
        @Nullable final List<TaskDTO> tasks2 = findAll();
        Assert.assertNotNull(tasks2);
        Assert.assertEquals(Collections.emptyList(), tasks2);
        Assert.assertEquals(0, tasks2.size());
        for (@NotNull TaskDTO task : tasks) {
            add(task);
        }
    }

}
