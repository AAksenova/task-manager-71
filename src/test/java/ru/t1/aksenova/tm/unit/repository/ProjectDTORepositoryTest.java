package ru.t1.aksenova.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.aksenova.tm.config.ServerConfiguration;
import ru.t1.aksenova.tm.entity.dto.ProjectDTO;
import ru.t1.aksenova.tm.entity.dto.TaskDTO;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.repository.dto.ProjectDTORepository;
import ru.t1.aksenova.tm.repository.dto.TaskDTORepository;
import ru.t1.aksenova.tm.util.UserUtil;

import java.util.List;

import static ru.t1.aksenova.tm.constant.ProjectTestData.*;
import static ru.t1.aksenova.tm.constant.TaskTestData.ALFA_TASK_NAME;
import static ru.t1.aksenova.tm.constant.TaskTestData.BETTA_TASK_NAME;
import static ru.t1.aksenova.tm.constant.UserTestData.TEST_LOGIN;
import static ru.t1.aksenova.tm.constant.UserTestData.TEST_PASSWORD;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ServerConfiguration.class})
public class ProjectDTORepositoryTest {

    @NotNull
    private static final ProjectDTO projectOne = new ProjectDTO(ONE_PROJECT_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @NotNull
    private static final ProjectDTO projectTwo = new ProjectDTO(TWO_PROJECT_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @NotNull
    private static final ProjectDTO projectThree = new ProjectDTO(THREE_PROJECT_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @NotNull
    private static final TaskDTO taskOne = new TaskDTO(ALFA_TASK_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @NotNull
    private static final TaskDTO taskTwo = new TaskDTO(BETTA_TASK_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @Nullable
    private String userId;

    @NotNull
    @Autowired
    private ProjectDTORepository projectRepository;

    @NotNull
    @Autowired
    private TaskDTORepository taskRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void setUp() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(TEST_LOGIN, TEST_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        projectOne.setUserId(userId);
        projectTwo.setUserId(userId);
        projectThree.setUserId(userId);
        projectRepository.save(projectOne);
        projectRepository.save(projectTwo);
        taskOne.setProjectId(projectOne.getId());
        taskOne.setUserId(userId);
        taskTwo.setProjectId(projectTwo.getId());
        taskTwo.setUserId(userId);
        taskRepository.save(taskOne);
        taskRepository.save(taskTwo);
    }

    @After
    public void tearDown() {
        taskRepository.delete(taskOne);
        taskRepository.delete(taskTwo);
        projectRepository.delete(projectOne);
        projectRepository.delete(projectTwo);
        projectRepository.delete(projectThree);
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    @Test
    public void findAllByUserIdTest() {
        @Nullable final List<ProjectDTO> projects = projectRepository.findAllByUserId(userId);
        Assert.assertNotNull(projects);
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));

    }

    @Test
    public void findByUserIdAndIdTest() {
        @Nullable final ProjectDTO project = projectRepository.findByUserIdAndId(userId, projectOne.getId()).get();
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getId(), projectOne.getId());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(2, projectRepository.countByUserId(userId));
    }

    @Test
    public void deleteAllByUserIdTest() {
        taskRepository.deleteAllByUserId(userId);
        projectRepository.deleteAllByUserId(userId);
        @Nullable final List<ProjectDTO> projects = projectRepository.findAllByUserId(userId);
        Assert.assertNotNull(projects);
        Assert.assertEquals(0, projects.size());
        projectRepository.save(projectOne);
        projectRepository.save(projectTwo);
        taskRepository.save(taskOne);
        taskRepository.save(taskTwo);
    }

}


