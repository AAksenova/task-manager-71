package ru.t1.aksenova.tm.unit.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.aksenova.tm.config.ApplicationConfiguration;
import ru.t1.aksenova.tm.config.WebApplicationConfiguration;
import ru.t1.aksenova.tm.entity.dto.TaskDTO;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.repository.dto.TaskDTORepository;
import ru.t1.aksenova.tm.util.UserUtil;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.t1.aksenova.tm.constant.ProjectTestData.STATUS_NOT_STARTED;
import static ru.t1.aksenova.tm.constant.TaskTestData.ALFA_TASK_NAME;
import static ru.t1.aksenova.tm.constant.TaskTestData.BETTA_TASK_NAME;
import static ru.t1.aksenova.tm.constant.UserTestData.TEST_LOGIN;
import static ru.t1.aksenova.tm.constant.UserTestData.TEST_PASSWORD;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        ApplicationConfiguration.class,
        WebApplicationConfiguration.class
})
public class TasksControllerTest {

    @NotNull
    private static final TaskDTO taskOne = new TaskDTO(ALFA_TASK_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @NotNull
    private static final TaskDTO taskTwo = new TaskDTO(BETTA_TASK_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @Nullable
    private String userId;

    private boolean reqAuth = false;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    @Autowired
    private TaskDTORepository taskRepository;

    @Before
    @SneakyThrows
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        authenticate();
        userId = UserUtil.getUserId();
        taskOne.setUserId(userId);
        taskTwo.setUserId(userId);
        taskRepository.save(taskOne);
        taskRepository.save(taskTwo);
    }

    @After
    public void tearDown() {
        if (reqAuth) {
            authenticate();
        }
        reqAuth = false;
        taskRepository.deleteAll();
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    private void authenticate() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(TEST_LOGIN, TEST_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        @NotNull final String url = "/tasks";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
        @Nullable final List<TaskDTO> tasks = taskRepository.findAllByUserId(userId);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }


}
