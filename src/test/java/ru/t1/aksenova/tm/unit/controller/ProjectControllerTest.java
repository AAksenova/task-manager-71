package ru.t1.aksenova.tm.unit.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.aksenova.tm.config.ApplicationConfiguration;
import ru.t1.aksenova.tm.config.WebApplicationConfiguration;
import ru.t1.aksenova.tm.entity.dto.ProjectDTO;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.repository.dto.ProjectDTORepository;
import ru.t1.aksenova.tm.util.UserUtil;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.t1.aksenova.tm.constant.ProjectTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.TEST_LOGIN;
import static ru.t1.aksenova.tm.constant.UserTestData.TEST_PASSWORD;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        ApplicationConfiguration.class,
        WebApplicationConfiguration.class
})
public class ProjectControllerTest {

    @NotNull
    private static final ProjectDTO projectOne = new ProjectDTO(ONE_PROJECT_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @NotNull
    private static final ProjectDTO projectTwo = new ProjectDTO(TWO_PROJECT_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @Nullable
    private String userId;

    private boolean reqAuth = false;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    @Autowired
    private ProjectDTORepository projectRepository;

    @Before
    @SneakyThrows
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        authenticate();
        userId = UserUtil.getUserId();
        projectOne.setUserId(userId);
        projectTwo.setUserId(userId);
        projectRepository.save(projectOne);
        projectRepository.save(projectTwo);
    }

    @After
    public void tearDown() {
        if (reqAuth) {
            authenticate();
        }
        reqAuth = false;
        projectRepository.delete(projectOne);
        projectRepository.delete(projectTwo);
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    private void authenticate() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(TEST_LOGIN, TEST_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Test
    @SneakyThrows
    public void createTest() {
        @NotNull final String url = "/project/create";
        @Nullable final List<ProjectDTO> projects = projectRepository.findAllByUserId(userId);
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @Nullable final List<ProjectDTO> projects2 = projectRepository.findAllByUserId(userId);
        Assert.assertNotNull(projects2);
        Assert.assertEquals(projects.size() + 1, projects2.size());
    }

    @Test
    @SneakyThrows
    public void deleteTest() {
        @NotNull final String url = "/project/delete/" + projectOne.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        Assert.assertNull(projectRepository.findByUserIdAndId(userId, projectOne.getId()).orElse(null));
    }

    @Test
    @SneakyThrows
    public void editTest() {
        @NotNull final String url = "/project/edit/" + projectOne.getId();
        projectOne.setDescription("Test Project 1");
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .flashAttr("project", projectOne))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @Nullable final ProjectDTO project = projectRepository.findByUserIdAndId(userId, projectOne.getId()).get();
        Assert.assertNotNull(project);
        Assert.assertEquals(projectOne.getName(), project.getName());
        Assert.assertEquals(projectOne.getDescription(), project.getDescription());
    }

}
