package ru.t1.aksenova.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.aksenova.tm.api.service.dto.ITaskDTOService;
import ru.t1.aksenova.tm.config.ServerConfiguration;
import ru.t1.aksenova.tm.entity.dto.TaskDTO;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.util.UserUtil;

import static ru.t1.aksenova.tm.constant.ProjectTestData.STATUS_NOT_STARTED;
import static ru.t1.aksenova.tm.constant.TaskTestData.ALFA_TASK_NAME;
import static ru.t1.aksenova.tm.constant.UserTestData.TEST_LOGIN;
import static ru.t1.aksenova.tm.constant.UserTestData.TEST_PASSWORD;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        ServerConfiguration.class
})
public class TaskDTOServiceTest {

    @NotNull
    private static final TaskDTO taskOne = new TaskDTO(ALFA_TASK_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @Nullable
    private String userId;

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void setUp() {
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(TEST_LOGIN, TEST_PASSWORD);
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        taskOne.setUserId(userId);
        taskOne.setUserId(userId);
        taskService.add(taskOne);
        taskService.add(taskOne);
    }

    @After
    public void tearDown() {
        taskService.removeOne(userId, taskOne);
    }

    @Test
    public void findByUserIdAndIdTest() {
        Assert.assertNotNull(taskService.findOneById(userId, taskOne.getId()));
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(taskService.getSize(userId), taskService.findAll(userId).size());
    }

    @Test
    public void remove() {
        taskService.removeOneById(userId, taskOne.getId());
        Assert.assertThrows(RuntimeException.class, () -> taskService.findOneById(userId, taskOne.getId()));
    }

}
