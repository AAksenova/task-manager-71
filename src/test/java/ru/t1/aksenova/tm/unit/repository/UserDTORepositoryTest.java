package ru.t1.aksenova.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.aksenova.tm.config.ServerConfiguration;
import ru.t1.aksenova.tm.entity.dto.UserDTO;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.repository.dto.UserDTORepository;

import static ru.t1.aksenova.tm.constant.UserTestData.TEST_LOGIN;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ServerConfiguration.class})
public class UserDTORepositoryTest {

    @NotNull
    @Autowired
    private UserDTORepository userRepository;

    @Test
    public void findByLoginTest() {
        @Nullable final UserDTO user = userRepository.findByLogin(TEST_LOGIN);
        Assert.assertNotNull(user);
        Assert.assertEquals(TEST_LOGIN, user.getLogin());
    }

}
