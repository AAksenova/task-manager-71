package ru.t1.aksenova.tm.integration.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.http.*;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;
import ru.t1.aksenova.tm.config.*;
import ru.t1.aksenova.tm.entity.dto.ProjectDTO;
import ru.t1.aksenova.tm.entity.dto.Result;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.marker.IntegrationCategory;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

import static ru.t1.aksenova.tm.constant.ProjectTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.AUTH_LOGIN_URL;
import static ru.t1.aksenova.tm.constant.UserTestData.AUTH_LOGOUT_URL;


@WebAppConfiguration
@Category(IntegrationCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        ApplicationConfiguration.class,
        ServerConfiguration.class,
        SecurityWebApplicationInitializer.class,
        ServiceAuthenticationEntryPoint.class,
        WebApplicationConfiguration.class
})
public class ProjectRestEndpointTest {

    @NotNull
    private static final ProjectDTO projectOne = new ProjectDTO(ONE_PROJECT_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @NotNull
    private static final ProjectDTO projectTwo = new ProjectDTO(TWO_PROJECT_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @NotNull
    private static final HttpHeaders header = new HttpHeaders();

    private static boolean reqAuth = false;

    @Nullable
    private static String sessionId;

    @BeforeClass
    public static void setUp() {
        reqAuth = authenticate();
        @NotNull final String url = PROJECT_URL + "add/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(projectTwo, header));
    }

    @AfterClass
    public static void tearDown() {
        if (!reqAuth) {
            authenticate();
        }
        @NotNull final String url = PROJECT_URL + "deleteById/" + projectTwo.getId();
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(projectTwo, header));
        reqAuth = false;
        logout();
    }

    private static boolean authenticate() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        Result result = new Result();
        @NotNull final ResponseEntity<Result> response = restTemplate.postForEntity(AUTH_LOGIN_URL, result, Result.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertNotNull(response.getBody());
        HttpHeaders headers = response.getHeaders();
        List<HttpCookie> cookies = java.net.HttpCookie.parse(headers.getFirst(HttpHeaders.SET_COOKIE));
        sessionId = cookies.stream().filter(item -> "JSESSIONID".equals(item.getName())).findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        header.put(HttpHeaders.COOKIE, Arrays.asList(("JSESSIONID=" + sessionId)));
        header.setContentType(MediaType.APPLICATION_JSON);
        return sessionId != null;
    }

    private static void logout() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.exchange(AUTH_LOGOUT_URL, HttpMethod.POST, new HttpEntity<>(header), Result.class);
    }

    private static ResponseEntity<ProjectDTO> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity
    ) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, ProjectDTO.class);
    }

    private static ResponseEntity<List> sendRequestList(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity
    ) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, List.class);
    }

    @Test
    public void addTest() {
        @NotNull final String url = PROJECT_URL + "add/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(projectOne, header));
        @NotNull final String urlFind = PROJECT_URL + "findById/" + projectOne.getId();
        Assert.assertNotNull(sendRequest(urlFind, HttpMethod.GET, new HttpEntity<>(header)).getBody());
    }

    @Test
    public void removeByIdTest() {
        @NotNull final String url = PROJECT_URL + "deleteById/" + projectOne.getId();
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(projectOne, header));
        @NotNull final String urlFind = PROJECT_URL + "findById/" + projectOne.getId();
        Assert.assertThrows(RuntimeException.class, () -> sendRequest(urlFind, HttpMethod.GET, new HttpEntity<>(header)).getStatusCodeValue());
    }

    @Test
    public void findAllTest() {
        @NotNull final String url = PROJECT_URL + "findAll";
        Assert.assertNotNull(sendRequestList(url, HttpMethod.GET, new HttpEntity<>(header)).getBody());
    }

    @Test
    public void findByIdTest() {
        @NotNull final String url = PROJECT_URL + "findById/" + projectTwo.getId();
        Assert.assertNotNull(sendRequest(url, HttpMethod.GET, new HttpEntity<>(header)).getBody());
    }

}
