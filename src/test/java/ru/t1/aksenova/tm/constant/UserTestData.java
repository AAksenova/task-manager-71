package ru.t1.aksenova.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public class UserTestData {

    @NotNull
    public final static String TEST_LOGIN = "test";

    @NotNull
    public final static String TEST_PASSWORD = "test";

    @NotNull
    public final static String AUTH_LOGIN_URL = "http://localhost:8080/api/auth/login?login=" + TEST_LOGIN + "&password=" + TEST_PASSWORD;

    @NotNull
    public final static String AUTH_LOGOUT_URL = "http://localhost:8080/api/auth/logout";
}
